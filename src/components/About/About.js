import React from 'react'
import{Link,Outlet} from 'react-router-dom'
import './About.css'

export default function About() {
  return (
    <div>
      <h1>A propos</h1>
      <div className='container-about'>
      <p className='txt-about'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam quibusdam ullam ratione eaque quia distinctio repellendus placeat accusamus dolore quam!</p>
      <nav className='sub-nav'>
      <div className="navbar-about"> 
        <Link to="/about/sketch/">Maquettes</Link>
        <Link to="/about/output">Productions</Link>
        </div>
      </nav>
      </div>
      <Outlet/>
    </div>
  )
}
