import React from 'react'
import './Home.css'
import BtnToggle from '../BtnToggle/BtnToggle';

export default function Home() {
  return (
    <div>
        <h1>Accueil</h1>
        <div className='container'>
          <BtnToggle />
        <p className='txt-home'>Lorem ipsum dolor sit,
         amet consectetur adipisicing elit. Cupiditate quidem inventore earum ipsum corrupti impedit voluptate, voluptas blanditiis iusto consequuntur quam vitae odio ut aut sed asperiores nihil culpa vero aperiam quisquam ad laboriosam perferendis sint. Dolore, illo quo nemo quibusdam sunt vero placeat enim quam in hic aut error.
        Lorem ipsum dolor sit, amet consectetur adipisicing elit.
         Cupiditate quidem inventore earum ipsum corrupti impedit voluptate,
          voluptas blanditiis iusto consequuntur quam vitae odio ut aut sed asperiores
           nihil culpa vero aperiam quisquam ad laboriosam perferendis sint. Dolore,
            illo quo nemo quibusdam sunt vero placeat enim quam in hic aut error.
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. 
      </p>
    </div>
    </div>
  )
}
