import React from 'react';
import { Link } from 'react-router-dom';
import './Navbar.css'; 

export default function Navbar() {
  return (
    <nav className="navbar"> 
      <div className="navbar-logo">Mon logo</div>
      <div className="navbar-links"> 
        <Link to="/" className="navbar-link">Acceuil</Link> 
        <Link to="about" className="navbar-link">A propos</Link>
        <Link to="contact" className="navbar-link">Contact</Link>
      </div>
    </nav>
  );
}